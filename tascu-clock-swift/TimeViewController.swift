import UIKit

class TimeViewController: ViewController {
    
    
    
    /*
     
     一秒ごとに処理が実行される関数
     
     
     */
    func update(tm: Timer) {
        
        /*
         
         現在の詳細日時を取得する
         
         
         */
        print("現在の詳細日時を取得する")
        let date = Date()
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        print("hours = \(hour):\(minutes):\(seconds)")
        
        
        
        /*
         
         針の角度取得
         
         
         */
        let hDeg = (hour % 12) * (360 / 12);
        let mDeg = minutes * (360 / 60);
        let sDeg = seconds * (360 / 60);
        
        print(hDeg)
        print(mDeg)
        print(sDeg)
        
        
        
        
        /*
         
         10より小さい場合に、「0」の文字を付け加えてラベルに代入
         
         
         */
        
        if (seconds < 10) {
            
            Whatseconds = "0" + String(seconds)
            
        }else{
            
            Whatseconds = String(seconds)
            
        }
        
        
        
        /*
         
         10より小さい場合に、「0」の文字を付け加えてラベルに代入
         
         
         */
        
        
        if (minutes < 10) {
            
            Whatminutes = "0" + String(minutes)
            
        }else{
            
            
            Whatminutes = String(minutes)
        }
        
        
        /*
         
         10より小さい場合に、「0」の文字を付け加えてラベルに代入
         
         
         */
        
        
        if (hour < 10) {
            
            Whathour = "0" + String(hour)
            
            print(Whatseconds!)
            
        }else{
            
            Whathour = String(hour)
            
        }
        
       secondsImage.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI * Double(sDeg) / 180.0));
}

}
