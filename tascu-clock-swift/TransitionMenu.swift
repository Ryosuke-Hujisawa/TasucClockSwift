//
//  TransitionMenu.swift
//  tascu-clock-swift
//
//  Created by ryosuke-hujisawa on 2017/05/15.
//  Copyright © 2017年 ryosuke-hujisawa. All rights reserved.
//

import UIKit

class TransitionMenu: UIView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        loadNib()
    }
    
    func loadNib(){
        let view = Bundle.main.loadNibNamed("TransitionMenu", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }

}
