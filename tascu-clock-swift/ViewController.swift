import UIKit
import Photos

var Whathour:String?
var Whatminutes:String?
var Whatseconds:String?




class ViewController: UIViewController {
    
    @IBOutlet weak var TimeAdjustmentButton: UIButton! 
    @IBOutlet weak var MatchingButton: UIButton!
    @IBOutlet weak var FitClook: UILabel!
    @IBOutlet weak var Setclook: UILabel!
    @IBOutlet weak var WhatHour: UILabel!
    @IBOutlet weak var WhatMinute: UILabel!
    @IBOutlet weak var WhatSeconds: UILabel!
    @IBOutlet weak var secondsImage: UIImageView!
    @IBOutlet weak var minuteImage: UIImageView!
    @IBOutlet weak var hourImage: UIImageView!
    @IBOutlet weak var BodyClockImage: UIImageView!
    @IBOutlet weak var ArrowDownUpButton: UIButton!
    @IBOutlet weak var TopMenu: UIView!
    
    
    var timer: Timer!
    var isClosed = 210
    var soundId:SystemSoundID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // システムサウンドへのパスを指定
        if let soundUrl:NSURL = NSURL(fileURLWithPath: "/Users/ryosukehujisawa/Desktop/TasucClockSwift/sound/collapse.wav") {
            
            AudioServicesCreateSystemSoundID(soundUrl, &soundId)
            AudioServicesPlaySystemSound(soundId)
        }


        UIView.animate(withDuration: 0.5,animations: { () -> Void in
        self.BodyClockImage.transform = self.BodyClockImage.transform.scaledBy(x: -1.0, y: 1.0)
        self.BodyClockImage.transform = self.BodyClockImage.transform.scaledBy(x: -1.0, y: 1.0)

        print("アナログ時計二回転した")
        })
        
        // 背景画像
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "HomeScreenBackground")?.draw(in: self.view.bounds)
        let image: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
        
        let win:UIWindow = UIApplication.shared.delegate!.window!!
        win.addSubview(TopMenu)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        timer.fire()
        
  
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        
        
        
        
        


        
        
        
        
        
        
        
        
        
        
        

        UIView.animate(withDuration: 0.6,
                       animations: {
                        self.TimeAdjustmentButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        self.MatchingButton.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.6) {
                            self.TimeAdjustmentButton.transform = CGAffineTransform.identity
                            self.MatchingButton.transform = CGAffineTransform.identity
                        }
        })
        
    }
    
    @IBAction func TimeAdjustmentViewButton(_ sender: Any) {
        let transition = CATransition()
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.navigationController!.view.layer.add(transition, forKey: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let HowToLeftPush = storyboard.instantiateViewController(withIdentifier: "Time") as! TimeViewController
        self.navigationController?.pushViewController(HowToLeftPush, animated: true )
        print("TimeViewControllerに遷移した")
    }
    
    @IBAction func MatchingViewButton(_ sender: Any) {
        let transition = CATransition()
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.navigationController!.view.layer.add(transition, forKey: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let HowToLeftPush = storyboard.instantiateViewController(withIdentifier: "Ring") as! RingViewController
        self.navigationController?.pushViewController(HowToLeftPush, animated: true )
        print("RingViewControllerに遷移した")
    }
    
    
    @IBAction func HomeBiewButton(_ sender: Any) {
        navigationController?.popToViewController(navigationController!.viewControllers[0], animated: true)
        print("一番最初の画面に戻った")
    }
    @IBAction func RingViewButton(_ sender: Any) {
        let transition = CATransition()
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.navigationController!.view.layer.add(transition, forKey: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let HowToLeftPush = storyboard.instantiateViewController(withIdentifier: "Ring") as! RingViewController
        self.navigationController?.pushViewController(HowToLeftPush, animated: true )
        print("RingViewControllerに遷移した")
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func SettingViewButton(_ sender: Any) {
        
        
        
        let transition = CATransition()
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.navigationController!.view.layer.add(transition, forKey: nil)
        let storyboard = UIStoryboard(name: "SettingView", bundle: nil)
        let HowToLeftPush = storyboard.instantiateViewController(withIdentifier: "SettingView") as! SettingViewController
        self.navigationController?.pushViewController(HowToLeftPush, animated: true )
        
        
        print("SettingViewControllerに遷移した")
    }
    
    
    
    
    
    
    @IBAction func TimeViewButton(_ sender: Any) {
        let transition = CATransition()
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.navigationController!.view.layer.add(transition, forKey: nil)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let HowToLeftPush = storyboard.instantiateViewController(withIdentifier: "Time") as! TimeViewController
        self.navigationController?.pushViewController(HowToLeftPush, animated: true )
        print("TimeViewControllerに遷移した")
    }
    func getCurrentViewController() -> UIViewController? {
        //UIViewの上に置いたUIbuttonから画面遷移するために必要な関数
        if let rootController = UIApplication.shared.keyWindow?.rootViewController {
            var currentController: UIViewController! = rootController
            while( currentController.presentedViewController != nil ) {
                currentController = currentController.presentedViewController
            }
            return currentController
        }
        return nil
    }
    @IBAction func TopMenuButton(_ sender: Any) {
        if (isClosed == -210){
            isClosed = 210
            print("TopMenuを引き上げた")
            //let buttonImageDefault :UIImage? = UIImage(named:"buttonDefault.png")
            
            ArrowDownUpButton.setImage(UIImage(named: "ArrowDown")?.withRenderingMode(.alwaysOriginal), for: .normal)
           
            //ArrowDownUpButton.setImage("buttonDefault.png", for: .Highlighted)
            
        }else if (isClosed == 210){
            isClosed = -210
            print("TopMenuを引き上げた")
            
            ArrowDownUpButton.setImage(UIImage(named: "ArrowUp")?.withRenderingMode(.alwaysOriginal), for: .normal)

            
            //let buttonImageSelected :UIImage? = UIImage(named:"buttonSelected.png")
            
        }
        let xPosition = TopMenu.frame.origin.x
        let yPosition = TopMenu.frame.origin.y - CGFloat(isClosed)
        let height = TopMenu.frame.size.height
        let width = TopMenu.frame.size.width
        UIView.animate(withDuration: 1.0, animations: {
        self.TopMenu.frame = CGRect(x:xPosition, y:yPosition, width:width, height:height)
        })
    }
    func update(tm: Timer) {
        //アナログ時計実装のための関数
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        let hDeg = (hour % 12) * (360 / 12);
        let mDeg = minutes * (360 / 60);
        let sDeg = seconds * (360 / 60)
        //print(hDeg)
        //print(mDeg)
        //print(sDeg)
        //10より小さい場合に、「0」の文字を付け加えてラベルに代入
        if (seconds < 10) {
            Whatseconds = "0" + String(seconds)
        }else{
            Whatseconds = String(seconds)
        }
        if (minutes < 10) {
            Whatminutes = "0" + String(minutes)
        }else{
            Whatminutes = String(minutes)
        }
        if (hour < 10) {
            Whathour = "0" + String(hour)
            //print(Whatseconds!)
        }else{
            Whathour = String(hour)
        }
          WhatHour.text = Whathour!
          WhatMinute.text = Whatminutes!
          WhatSeconds.text = Whatseconds!

 
        secondsImage.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI * Double(sDeg) / 180.0))
        
        minuteImage.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI * Double(mDeg) / 180.0))
        
        hourImage.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI * Double(hDeg) / 180.0))
        
        
        
}
    
    
    
    
        
    
}














